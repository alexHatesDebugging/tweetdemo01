package com.kenfogel.tweetdemo01.presentation;

import com.kenfogel.tweetdemo01.business.TwitterEngine;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * Based on code from https://www.baeldung.com/twitter4j
 *
 * @author Ken Fogel
 */
public class TweetDemo01 extends Application {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(TweetDemo01.class);
    
    /**
     * Where it begins
     * @param args 
     */
    public static void main(String[] args) {
        launch(args);
    }
    private String errorMessage="This message is too long make it 280 characters or less";
    private String handleMesaage="Enter Handle";
    private TextArea textAreaTweeter;
    private TextArea textAreaMessanger;
    private final TwitterEngine twitterEngine = new TwitterEngine();
    private BorderPane messangerPane;
    private BorderPane tweeterPane;
    
    /**
     * JavaFX begins at start
     * 
     * @param primaryStage
     * @throws Exception 
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        // Set window's title
        primaryStage.setTitle("Send a tweet");
        
        StackPane root = new StackPane();
        this.messangerPane = makeDirectMessanger();
        this.tweeterPane = makeTweeterMessanger();
        root.getChildren().add(tweeterPane);
        root.getChildren().add(messangerPane);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }
    private BorderPane makeTweeterMessanger(){
        BorderPane tweeterMessangerPain = new BorderPane();
        tweeterMessangerPain.setBottom(makeTweetButtons());
        tweeterMessangerPain.setTop(switchTweeterToMessanger());
        textAreaTweeter = new TextArea();
        textAreaTweeter.setWrapText(true);
        tweeterMessangerPain.setCenter(textAreaTweeter);
        tweeterMessangerPain.setVisible(false);
        return tweeterMessangerPain;
    }
    private BorderPane makeDirectMessanger(){
        BorderPane directMessangerPain = new BorderPane();
        textAreaMessanger = new TextArea();
        textAreaMessanger.setWrapText(true);
        TextField textField = new TextField(); 
        Label label = new Label(this.handleMesaage);
        HBox hbox = new HBox();
        Button exit = new Button("Exit");
        exit.setOnAction(event -> Platform.exit());
        hbox.getChildren().add(label);
        hbox.getChildren().add(textField);
        hbox.getChildren().add(switchMessangerToTweeter());
        hbox.setAlignment(Pos.CENTER_LEFT);
        hbox.setSpacing(20.0);
        hbox.setPadding(new Insets(10, 10, 10, 10));
        directMessangerPain.setBottom(makeMessangerButtons(textField));
        directMessangerPain.setTop(hbox);
        directMessangerPain.setCenter(textAreaMessanger);
        directMessangerPain.setVisible(true);
        return directMessangerPain;
    }
    private Button switchTweeterToMessanger(){
        Font font = new Font(18);
        Button switchToMessangerButton = new Button("Messanger");
        switchToMessangerButton.setFont(font);
        switchToMessangerButton.setOnAction(event -> switchToMessanger());
        return switchToMessangerButton;
    }
    
    private Button switchMessangerToTweeter(){
        Font font = new Font(10);
        Button switchToTweeterButton = new Button("Tweeter");
        switchToTweeterButton.setFont(font);
        switchToTweeterButton.setOnAction(event -> switchToTweeter());
        return switchToTweeterButton;
    }
    /**
     * Create an HBox of buttons
     * 
     * @return the constructed hbox
     */
    private HBox makeTweetButtons() /*throws TwitterException*/{
        HBox hbox = new HBox();
        Button send = new Button("Send");
        send.setOnAction(event -> {
            scanTweet();
        });
        Font font = new Font(18);
        send.setFont(font);

        Button exit = new Button("Exit");
        exit.setOnAction(event -> Platform.exit());
        exit.setFont(font);

        hbox.getChildren().add(send);
        hbox.getChildren().add(exit);

        hbox.setAlignment(Pos.CENTER);
        hbox.setSpacing(20.0);
        hbox.setPadding(new Insets(10, 10, 10, 10));
        return hbox;
    }
    private void scanTweet(){
        try {
                String test = textAreaTweeter.getText();
                if(test.length()>280){
                    sendError(textAreaTweeter);
                }
                else{
                twitterEngine.createTweet(test);
                 }
            } catch (TwitterException ex) {
                LOG.error("Unable to send tweet", ex);
            }
    }
    private HBox makeMessangerButtons(TextField textfeild) /*throws TwitterException*/{
        HBox hbox = new HBox();
        Button send = new Button("Send");
        send.setOnAction(event -> {
            scanMessage(textfeild);
        });
        Font font = new Font(18);
        send.setFont(font);

        Button exit = new Button("Exit");
        exit.setOnAction(event -> Platform.exit());
        exit.setFont(font);

        hbox.getChildren().add(send);
        hbox.getChildren().add(exit);

        hbox.setAlignment(Pos.CENTER);
        hbox.setSpacing(20.0);
        hbox.setPadding(new Insets(10, 10, 10, 10));
        return hbox;
    }
    private void scanMessage(TextField textfeild){
        try {
                String test = textAreaMessanger.getText();
                String handle=textfeild.getText();
                if(test.length()>280){
                    sendError(textAreaMessanger);
                }
                else{
                twitterEngine.sendDirectMessage(handle,test);
                 }
            } catch (TwitterException ex) {
                LOG.error("Unable to send tweet", ex);
            }
    }
    private void switchToTweeter(){
        this.messangerPane.setVisible(false);
        this.tweeterPane.setVisible(true);
    }
    private void switchToMessanger(){
        this.messangerPane.setVisible(true);
        this.tweeterPane.setVisible(false);
    }
    private void sendError(TextArea textArea){
        textArea.setText(this.errorMessage);
    } 

}
